package main

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/russross/blackfriday/v2"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Posts struct {
	Posts []Post `json:"posts" bson:"posts"`
}

type Post struct {
	UserId int    `json:"userId" bson:"userId"`
	Id     int    `json:"id" bson:"id"`
	Title  string `json:"title" bson:"title"`
	Body   string `json:"body" bson:"body"`
}

type Site struct {
	port  string
	gin   *gin.Engine
	mongo *mongo.Client
}

func (s *Site) start() {
	srv := &http.Server{
		Addr:           s.port,
		Handler:        s.gin,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(srv.ListenAndServe())
}

func (s *Site) json(c *gin.Context) {
	options := options.Find()
	options.SetSort(bson.D{{"id", -1}})
	options.SetLimit(7)

	db := s.mongo.Database("site")
	postsCollection := db.Collection("posts")
	cursor, err := postsCollection.Find(c.Request.Context(), bson.D{}, options)
	if err != nil {
		log.Fatal(err)
	}

	var posts []bson.M
	if err = cursor.All(c.Request.Context(), &posts); err != nil {
		log.Fatal(err)
	}

	c.JSON(http.StatusOK, posts)
}

func (s *Site) postsHash(c *gin.Context) {
	db := s.mongo.Database("site")
	postsCollection := db.Collection("posts")
	cursor, err := postsCollection.Find(c.Request.Context(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	var posts []bson.M
	if err = cursor.All(c.Request.Context(), &posts); err != nil {
		log.Fatal(err)
	}

	jsonResp, _ := json.Marshal(posts)
	sum := md5.Sum([]byte(string(jsonResp)))
	hex := hex.EncodeToString(sum[:])
	c.Data(http.StatusOK, "text", []byte(hex))

}

func (s *Site) posts(c *gin.Context) {
	postid := c.Param("postid")
	markdown, err := ioutil.ReadFile("pages/" + postid + ".md")
	if err != nil {
		fmt.Println(err)
	}
	html := blackfriday.Run(markdown)
	c.Data(http.StatusOK, "text/html; charset=utf-8", html)
}

func (s *Site) routes() {
	s.gin.GET("/json", s.json)
	s.gin.GET("/jsonHash", s.postsHash)
	s.gin.GET("/markdown/:postid", s.posts)
}

func main() {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://127.0.0.1:27017"))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()

	api := Site{
		port:  ":7878",
		gin:   gin.Default(),
		mongo: client,
	}

	api.gin.Use(cors.Default())
	api.routes()
	api.start()
}
