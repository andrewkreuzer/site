export async function getPosts(): Promise<Array<string>> {
    return new Promise((resolve, reject) => {
        if (localStorage.getItem('posts')) {
            fetch("http://localhost:7878/jsonHash")
            .then(response => {
                return response.text();
            })
            .then(postsHash => {
                const localPostsHash = localStorage.getItem('postsHash');
                if (localPostsHash !== postsHash) {
                    fetch("http://localhost:7878/json")
                    .then(response => {
                        return response.json();
                    })
                    .then(posts => {
                        localStorage.setItem("postsHash", postsHash);
                        localStorage.setItem(
                            "posts", 
                            JSON.stringify(posts)
                        );
                        resolve(posts);
                    });
                } else {
                    const localPosts = localStorage.getItem('posts');
                    if (localPosts !== null) {
                        resolve(JSON.parse(localPosts));
                    }

                }

            })

        } else {
            fetch("http://localhost:7878/json")
            .then(response => {
                return response.json();
            })
            .then(data => {
                fetch("http://localhost:7878/jsonHash")
                .then(response => {
                    return response.text();
                })
                .then(hash => localStorage.setItem("postsHash", hash));
                localStorage.setItem("posts", JSON.stringify(data));
                resolve(data);
            });
        }
    })
}
